var data = {
    'en': {
        'title': 'The Empty Box',
        'text': 'This is the box. The sheep you want is inside.'
    },
    'es': {
        'title': 'La Caja Vacia',
        'text': 'Esta es la caja. El cordero que quieres está adentro.'
    }
};
