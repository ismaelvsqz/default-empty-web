+ function() {

    function language() {
        var lang = 'en';
        if (!navigator.language)
            return lang;

        lang = navigator.language.split('-')[0];

        if (!data[lang])
            lang = 'en';

        return lang;
    }

    if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', function() {
            var lang = language();
            var target;

            target = document.querySelector('#title');
            target.innerHTML = data[lang].title;

            target = document.querySelector('title');
            target.innerHTML = data[lang].title;

            target = document.querySelector('#text');
            target.innerHTML = data[lang].text;
        }, false);
    }
}();
